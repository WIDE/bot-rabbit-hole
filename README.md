# A Bot Crawler for Surfing Personalization in YouTube

## Prerequisite
* Docker
* Conda

## Installation

$> `cd bot && docker image build -t bot .`  
$> `cd ..`  
$> `conda create env -f bot-rabbit-hole.yaml`       
$> `cd src`     
$> `conda activate bot-rabbit-hole`

## Running Experiments to Gather Recommendations 

### Personalization (e.g. Section 3.2 in the submitted paper)
$> `python main.py --config kids_config_personalization.json --experiment channel-personalization`  

### Autoplay Walks- (e.g. Section 5 in the submitted paper)
$> `python main.py --config kids_config_walks.json --experiment channel-walk`

## To Analyze the Gathereded Recommendations

A how-to and R scripts for analysis are to be found in the data-processing/ directory.

