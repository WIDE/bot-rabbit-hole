# Bot Crawler for Surfing Personalization in YouTube: Data exploitation part

## Prequisites:
* R
* Tidyverse (mandatory)
* data.table (mandatory, to import files)
* ggdendro (only to plot dendrograms)

## usage:

* Unpack vldb-data, it should lead to a data-vldb directory. This is a subsampled dataset (only contains 1/10 observations for validation purposes). Full data (hefty) available upon request.
* These scripts read csv data extracted from the bots json logs (easier to ingest in R). 
* Run any script within vldb directory
* All scripts output pdfs in the vldb/pdf directory
* Code is guaranteed without exception handling: any deviation from expected behaviour will likely have the script miserably fail. 

## vldb/validationchild.R
   This script focuses on the validation part (which relies on child labeling). It generates fig 2.

## vldb/heatMapDendro.R
   This script constructs the heatmap of the profiles, and uses the constructed similarity matrix to produce the corresponding dendrogram. It generates fig. 5

## vldb/cluster.R
   This script focuses on the kmeans approach to RH detection.  It generates fig 6,7 and table 3 of the paper
