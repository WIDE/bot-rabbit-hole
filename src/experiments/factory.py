from abc import ABC, abstractmethod

from experiments.base import ExperimentNotFound
from experiments.generic import ChannelWalkExperiment, ChannelPersonalizationExperiment


class ExperimentFactory(ABC):
    """
    Abstract Factory class for experiments, generate experiment suites regarding experiment name and naming
    """

    @abstractmethod
    def create_experiment(self, **kwargs): """
    Abstract method for creating experiments, 
        :param kwargs: Arguments
        :return: 
        """


class GenericExperimentFactory(ExperimentFactory):
    """
    Factory class for generating walk or personalization experiment.
    """
    def create_experiment(self, **kwargs):
        """
        Generate walk or personalization experiment using "experiment" key on given **kwargs"
        :param kwargs: Experiment configuration, "experiment" key has to be valid.
        :return: Experiment class.
        """
        experiment_name = kwargs['experiment']
        if experiment_name == "channel-walk":
            return ChannelWalkExperiment(**kwargs)
        elif experiment_name == "channel-personalization":
            return ChannelPersonalizationExperiment(**kwargs)
        else:
            raise ExperimentNotFound
