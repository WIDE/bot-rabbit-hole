import logging
import random

from experiments.adapter import WalkExperimentAdapter, ChannelPersonalizationAdapter


class ChannelWalkExperiment(WalkExperimentAdapter, ChannelPersonalizationAdapter):
    """
    Walk Epxeriment.
    Aim is to start watching a video from a random channels inside the given channel lists,
    autoplaying and fetching recommendations.
    """
    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.channels = kwargs['channels']

    def run(self):
        channels_videos, channels_type = self.fill_channels_videos()
        iteration = 0
        while True:
            iteration += 1
            channel = random.choice(list(channels_videos))
            rvk = random.choice(channels_videos[channel])
            filename = "autoplay-k-" + channels_type[channel] + "-" + channel + "_" + str(self.nb_walks) + "_" + str(
                random.randint(1, 10000))
            self.walk_over_video(filename, rvk)

            logging.info(f"Number of runs: {iteration:d}, filename: {filename}")


class ChannelPersonalizationExperiment(ChannelPersonalizationAdapter):
    """
    Personalization experiment.
    Aim is to fetch random channels from kids channels, playing their videos and fetching recommendations.
    As by watching videos more and more on the same content, personalized recommendations are expected.
    """

    def __init__(self, **kwargs) -> None:
        super().__init__(**kwargs)
        self.channels = kwargs['channels']
        self.channel_sniffing_reload_hour = kwargs['channel_sniffing_reload_hour']

    def run(self):
        channels_videos, channels_type = self.fill_channels_videos()
        iteration = 0
        while True:
            iteration += 1
            channel = random.choice(list(channels_videos))
            logging.info(f"Chosen channel: {channel}")
            random_videos_from_channel = random.choices(channels_videos[channel], k=self.nb_walks)

            start = [{"video_id": rvk["url"].replace("https://www.youtube.com/watch?v=", ""),
                      "searchTerm": self.clean_space_regex.sub(" ", rvk["title"]), "tag": channel} for rvk in
                     random_videos_from_channel]
            filename = "autoplay-k-" + channels_type[channel] + "-" + channel + "_" + str(self.nb_walks) + "_" + str(
                random.randint(1, 10000))
            self.run_personalized_channel_walk(filename, start)
            logging.info(f"Number of runs: {iteration:d}, filename: {filename}")

