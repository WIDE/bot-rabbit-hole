import json
import subprocess
import tempfile

from utils import config


def runJob(events, userAgentResoluton, cookies, lang=None):
    """
    Generate docker run command by the parameters provided.
    :param events: Events for watching video
    :param userAgentResoluton: Browser settings
    :param cookies: Cookies (optional)
    :param lang: Language for browser
    :return: Extracted recommendations
    """
    user_agent = userAgentResoluton['user_agent']
    width = userAgentResoluton['width']
    height = userAgentResoluton['height']
    events_string = "\'" + "\' \'".join(events) + "\'"
    args = '-e ' + events_string
    if width and height:
        args = args + ' -wr ' + str(width)
        args = args + ' -hr ' + str(height)
    if user_agent:
        args = args + " -u \'" + str(user_agent) + "\'"
    if cookies:
        args = args + " -c " + str(cookies)
    if lang:
        args = args + " -l " + str(lang)

    pwd_cookies = config.get_cookies()

    command = "docker container run --rm -v " + pwd_cookies + ":/home/node/app/input bot " + args
    print(command)

    with tempfile.NamedTemporaryFile(mode="w+", delete=True) as file:  # save docker output to temp file,
        subprocess.check_call(command, stdout=file, shell=True)
        file.seek(0)  # sync. with disk
        read = file.read()  # receive bot output
    try:
        output = json.loads('[' + str(read)[:-2] + ']')
    except:
        print(read)
        output = []
    return output
