import json
import pickle
import re
from datetime import datetime
from io import StringIO

import pandas as pd

from utils import config


def read_json(path):
    """
    Reads Json from given path
    :param path: Input path
    :return: Json Object
    """
    with open(path) as f:
        return json.load(f)


def read_pickle(path):
    """
    Reads pickle from given path
    :param path: Input path
    :return: Pickle Object
    """
    with open(path, "rb") as f:
        return pickle.load(f)


def dump_json(obj, path):
    """
    Saves json object on given path
    :param obj: Json Serializable Python Object
    :param path: Output path
    """
    with open(path, "w") as f:
        json.dump(obj, f)


def dump(vars, res, filename, store_folder=None):
    """
    Dumps global variables and results to json, csv and zip files.
    :param store_folder: which folder to store, default None redirects to config.OUTPUT
    :param vars: Global variables. Results and html's are deleted before export
    :param res: Bot response
    :param filename: Base name to export files
    :return: csv_path
    """
    out_file_base_name = config.get_output_base_name(filename, store_folder)
    csv_path = save_csv(res, filename, store_folder)

    with open(out_file_base_name + "_vars.txt", 'w') as f:
        if "html_json" in vars.keys():
            vars.pop("html_json")
        if "res" in vars.keys():
            vars.pop("res")
        if "result" in vars.keys():
            vars.pop("result")
        f.write(str(vars))
    return csv_path


def save_csv(set, filename, store_folder=None):
    """
    Saves result as csv
    :param set: set Vidéos JSON list
    :param filename: Nom du fichier csv
    :return: Chemin du fichier csv
    """
    dateTimeObj = datetime.now()
    if filename == None:
        filename = str(dateTimeObj)
    return exportSet(set, filename, store_folder)


def exportSet(set, filename, store_folder=None):
    """
    Export recommendations to given path as CSV
    :param set: Recommendations
    :param filename: Filename to export.
    :param store_folder: Custom output folder
    :return: exported path
    """
    flat_list = getVideos(set)
    df = pd.read_json(StringIO(json.dumps(flat_list)), orient='records')

    if not df.empty:
        csv_path = config.get_output_base_name(filename, store_folder) + '.csv'
        df.to_csv(csv_path, index=None,
                  columns=["url", "homePosition", "ytkids", "type", "insertionDate", "refreshNB", "regionAllowed",
                           "author", "watchTime", "actionNB", "videoViewsNB", "parent_id"])
        return csv_path
    else:
        return None


def getVideos(videos):
    """
    Post-processing on recommendations list before export
    :param videos: Recommendations
    :return: Post-processed recommendations list
    """
    for video in videos:
        url = video['url']
        m = re.search('(?<=v=).*(?<=&)', url)
        if m is None:
            video_id = url[url.rfind('v=') + 2:]
        else:
            t = m.group(0)
            video_id = t[:-1]
        video['ytkids'] = 'NA'
        video['regionAllowed'] = ''
        video['url'] = video_id
        parent = video['parent_id']
        video['parent_id'] = parent[parent.rfind('v=') + 2:]
    return videos
