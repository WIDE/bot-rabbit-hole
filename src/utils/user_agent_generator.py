import json
import random

import pandas as pd

from utils import config
from utils.config import USER_AGENT_CSV, INPUT_RESOLUTION_CSV


def generate_resolution():
    """
    Generates random browser resolution from predefined resolutions for realistic bot.
    :return: Browser resolution.
    """
    df = pd.read_csv(config.get_path(INPUT_RESOLUTION_CSV))
    resolution_list = json.loads(
        df.to_json(orient="records", date_format="epoch", double_precision=10, force_ascii=True, date_unit="ms",
                   default_handler=None))
    resolution = random.choice(resolution_list)
    return resolution


def generate_user_agent():
    """
    Generates random browser user-agent from predefined user-agents for realistic bot
    :return: User-agent.
    """
    df = pd.read_csv(config.get_path(USER_AGENT_CSV))
    user_agent_list = json.loads(
        df.to_json(orient="records", date_format="epoch", double_precision=10, force_ascii=True, date_unit="ms",
                   default_handler=None))
    user_agent = random.choice(user_agent_list)
    return user_agent


def generate_user_agent_and_resolution():
    """
    Generates random browser resolution and user-agent configurations from defined settings for realistic bot.
    :return: Resolution and user-agent dictionary.
    """
    user_agent = generate_user_agent()
    resolution = generate_resolution()
    return {'user_agent': user_agent['user_agent'], 'width': resolution['width'], 'height': resolution['height']}
