import {Job} from "./job";
import {Action} from '../abstract/action'
import {YoutubeAction} from '../module/youtube/youtubeAction'

export class Fetch extends Job{

    constructor(page:any, args:any, refreshNB:number, videoViewsNB: number){
      super()
      this.action = new YoutubeAction(page,args)
      this.action.setRefreshNB(refreshNB)
      this.action.videoViewsNB = videoViewsNB
    }
    async execute(){
      this.action.actionNB = this.action.actionNB + 1;
      for (let index = 0; index < this.action.searchSelection; index++) {
        await this.action.clickHome()
        await this.action.getFullHomepage()
        this.action.refreshNB = this.action.refreshNB + 1;
      }
      return 0;
    }
}
